import { Injectable } from '@angular/core';
import { Cube } from './Class/cube';
import { Cell } from './Class/cell';
import { RightMove } from './Class/right-move';
import { UpMove } from './Class/up-move';
import { TopRightMovement } from './Class/top-right-movement';
import {CubeHelperService} from '../app/cube-helper.service';

@Injectable({
  providedIn: 'root'
})
export class ServiceCube2Service {
  private rightMove:RightMove = new RightMove();
  private upMove   :UpMove    = new UpMove();
  private topRight :TopRightMovement = new TopRightMovement();
  private helperService = new CubeHelperService();
  public stepsList : Cube[];
  public stepIndex :number;
  
  constructor(public cube:Cube) {
    this.readStepsList();
    this.stepIndex = -1;
  }

  Core(mode:string, direction:string, number:number, face:string){
    if (mode == 'Rows') {
      if (face == 'cube show-top') {
        if (direction == 'Right') {
            this.topRightMovement(number);
        }
        else{
          for (var i = 0; i < 3; ++i) {
            this.topRightMovement(number);
          }
        }
      }
      else if (face == 'cube show-bottom') {
        if (direction == 'Left') {
          if (number == 0) {
            this.topRightMovement(2);
          }
          else if (number == 2) {
            this.topRightMovement(0);
          }
          else{
            this.topRightMovement(number);
          }
        }
        else{
          for (var i = 0; i < 3; ++i) {
            if (number == 0) {
            this.topRightMovement(2);
            }
            else if (number == 2) {
            this.topRightMovement(0);
            }
            else{
            this.topRightMovement(number);
            }
          }
        }
      }
      else if(direction == 'Left'){
        for (var i = 0; i < 3; ++i) {
          this.rightMovement(number);
        }
      }
      else if(direction == 'Right'){
        this.rightMovement(number);
      }
    }
    else{
      if (face == 'cube show-left') {
        if (direction == 'Up') {
          this.topRightMovement(number);
        }
        else{
          for (var i = 0; i < 3; ++i) {
            this.topRightMovement(number);
          }
        }
      }
      else if (face == 'cube show-right') {
        if (direction == 'Down') {
          if (number == 0) {
            this.topRightMovement(2);
          }
          else if (number == 2) {
            this.topRightMovement(0);
          }
          else{
            this.topRightMovement(number);
          }
        }
        else{
          for (var i = 0; i < 3; ++i) {
            if (number == 0) {
              this.topRightMovement(2);
            }
            else if (number == 2) {
              this.topRightMovement(0);
            }  
            else{
              this.topRightMovement(number);
            }
          }
        }
      }
      else if (face == 'cube show-back') {
        if (direction == 'Down') {
          if (number == 0) {
            this.upMovement(2);
          }
          else if (number == 2) {
            this.upMovement(0);
          }  
          else{
            this.upMovement(number);
          }
        }
        else{
          for (var i = 0; i < 3; ++i) {
            if (number == 0) {
              this.upMovement(2);
            }
            else if (number == 2) {
              this.upMovement(0);
            }  
            else{
              this.upMovement(number);
            }
          }
        }
      }
      else if (direction == 'Up') {
        this.upMovement(number);
      }
      else{
        for (var i = 0; i < 3; ++i) {
          this.upMovement(number);
        }
      }
    }
    this.checkTraspose(mode,direction,number,face);
    this.saveSteps();
  }


  readStepsList(){
    let jsonResult = <Cube[]>JSON.parse(localStorage.getItem("stepsList"));
    this.stepsList = jsonResult != null ? jsonResult : [];
  }

  getLastIndexStep(){
    this.readStepsList();
    return this.stepsList.length - 1;
  }
  
  saveSteps(){
    this.readStepsList();
    //if(newStep.index == -1){
      //New movement added
      this.stepsList.push(this.cube);
      localStorage.setItem("stepsList",JSON.stringify(this.stepsList));
    /*}
    else{
      //Affect old movements
      this.stepsList[newStep.index] = newStep;
      let top = newStep.index;
      let cont = 0;
      let result = [];
      while(cont <= top){
        result.push(this.stepsList[cont]);
        cont++;
      }
      this.stepsList = result;
    }*/
  }

  setOriginalState(){
    this.stepsList = [];
    localStorage.setItem("stepsList",JSON.stringify(this.stepsList));
  }
  getMovement(index:number){
    this.readStepsList();
    return index > -1 ? this.stepsList[index] : null;
  }


  rightMovement(row:number){
      let original = this.cube.faceList[this.rightMove.star].cellMatrix[row];
      this.rightMove.movements.forEach(move => {
        this.cube.faceList[move.to].cellMatrix[row] = this.cube.faceList[move.from].cellMatrix[row];
      });
      this.cube.faceList[this.rightMove.end].cellMatrix[row] =  original;
  }

  topRightMovement(row:number){
    let original;
    if (row == 0) {
      original = this.getColumns(2,this.topRight.star)
    }
    else if (row == 2) {
      original = this.getColumns(0,this.topRight.star)
    }
    else{
      original = this.getColumns(row,this.topRight.star)
    }
    this.topRight.movements.forEach(move => {
      if (move.from == 4 || move.from == 5) {
        if (move.to == 2) {
          if (row == 0) {
            this.setColumns(2,move.to,this.cube.faceList[move.from].cellMatrix[row]);
          }
          else if (row == 2) {
            this.setColumns(0,move.to,this.cube.faceList[move.from].cellMatrix[row]);
          }
          else{
            this.setColumns(row,move.to,this.cube.faceList[move.from].cellMatrix[row]); 
          }
        }
        else if (move.to == 3){
          if (row == 0) {
            this.setColumns(row,move.to,this.cube.faceList[move.from].cellMatrix[2]);
          }
          else if (row == 2) {
            this.setColumns(row,move.to,this.cube.faceList[move.from].cellMatrix[0]);
          }
          else{
            this.setColumns(row,move.to,this.cube.faceList[move.from].cellMatrix[row]);
          }
        }
        else{
          this.setColumns(row,move.to,this.getColumns(row,move.from))
        }
      }
      else{
        this.cube.faceList[move.to].cellMatrix[row] = this.getColumns(row,move.from);
      }
      });
      if (row == 2) {
        this.cube.faceList[this.topRight.end].cellMatrix[0] =  original;
      }
      else if(row == 0){
        this.cube.faceList[this.topRight.end].cellMatrix[2] =  original;
      }
      else{
        this.cube.faceList[this.topRight.end].cellMatrix[row] =  original;
      }
  }

  upMovement(column:number){
    let original = this.getColumns(column,this.upMove.star)
    this.upMove.movements.forEach(move => {
        this.setColumns(column,move.to,this.getColumns(column,move.from));
      });
    this.setColumns(column,this.upMove.end,original);
 }
 getColumns(column:number,facenumber:number){
   if (facenumber == 1 && column == 0) {
     return [
      this.cube.faceList[facenumber].cellMatrix[0][column + 2],
      this.cube.faceList[facenumber].cellMatrix[1][column + 2],
      this.cube.faceList[facenumber].cellMatrix[2][column + 2]
    ];
   }
   else if (facenumber == 1 && column == 2) {
     return [
      this.cube.faceList[facenumber].cellMatrix[0][column - 2],
      this.cube.faceList[facenumber].cellMatrix[1][column - 2],
      this.cube.faceList[facenumber].cellMatrix[2][column - 2]
      ];
   }
   else{
      return [
      this.cube.faceList[facenumber].cellMatrix[0][column],
      this.cube.faceList[facenumber].cellMatrix[1][column],
      this.cube.faceList[facenumber].cellMatrix[2][column]
    ];
   }
 }

 setColumns(column:number,facenumber:number,cellList:any[]){
    for (let index = 0; index < 3; index++) {
      if (facenumber == 1 && column == 0) {
        this.cube.faceList[facenumber].cellMatrix[index][column + 2] = cellList[index];
      }
      else if (facenumber == 1 && column == 2) {
        this.cube.faceList[facenumber].cellMatrix[index][column - 2] = cellList[index];
      }
      else{
        this.cube.faceList[facenumber].cellMatrix[index][column] = cellList[index];
      }
    }
 }
 checkTraspose(mode:string, direction:string, number:number,face:string){

   if (face == 'cube show-top') {
     if (mode == 'Rows') {
       if (direction == 'Right') {
         if (number == 0) {
           this.rotateMatrix(this.cube.faceList[1].cellMatrix,'L');
         }
         if (number == 2) {
           this.rotateMatrix(this.cube.faceList[0].cellMatrix,'R');
         }
       }
       else{
         if (number == 0) {
           this.rotateMatrix(this.cube.faceList[1].cellMatrix,'R');
         }
         if (number == 2) {
           this.rotateMatrix(this.cube.faceList[0].cellMatrix,'L');
         }
       }
     }
     else{
       if (direction == 'Up') {
         if (number == 0) {
           this.rotateMatrix(this.cube.faceList[3].cellMatrix,'L');
         }
         if (number == 2) {
           this.rotateMatrix(this.cube.faceList[2].cellMatrix,'R');
         }
        }
        else{
          if (number == 0) {
           this.rotateMatrix(this.cube.faceList[3].cellMatrix,'R');
         }
         if (number == 2) {
           this.rotateMatrix(this.cube.faceList[2].cellMatrix,'L');
         }
        }
       
     }
   }
   else if (face == 'cube show-bottom') {
     if (mode == 'Rows') {
       if (direction == 'Right') {
         if (number == 0) {
           this.rotateMatrix(this.cube.faceList[0].cellMatrix,'L');
         }
         if (number == 2) {
           this.rotateMatrix(this.cube.faceList[1].cellMatrix,'R');
         }
       }
       else{
         if (number == 0) {
           this.rotateMatrix(this.cube.faceList[0].cellMatrix,'R');
         }
         if (number == 2) {
           this.rotateMatrix(this.cube.faceList[1].cellMatrix,'L');
         }
       }
     }
     else{
       if (direction == 'Up') {
         if (number == 0) {
           this.rotateMatrix(this.cube.faceList[3].cellMatrix,'L');
         }
         if (number == 2) {
           this.rotateMatrix(this.cube.faceList[2].cellMatrix,'R');
         }
        }
        else{
          if (number == 0) {
           this.rotateMatrix(this.cube.faceList[3].cellMatrix,'R');
         }
         if (number == 2) {
           this.rotateMatrix(this.cube.faceList[2].cellMatrix,'L');
         }
        }
      }
   }
   else if (mode == 'Rows') {
     if (direction == 'Right') {
         if (number == 0) {
           this.rotateMatrix(this.cube.faceList[4].cellMatrix,'L');
         }
         if (number == 2) {
            this.rotateMatrix(this.cube.faceList[5].cellMatrix,'R');
         }
      }
      else{
        if (number == 0) {
           this.rotateMatrix(this.cube.faceList[4].cellMatrix,'R');
         }
         if (number == 2) {
            this.rotateMatrix(this.cube.faceList[5].cellMatrix,'L');
         }

      }  
   }
   else{
    if (face == 'cube show-right') {
       if (mode == 'Columns') {
         if (direction == 'Up') {
           if (number == 0) {
             this.rotateMatrix(this.cube.faceList[0].cellMatrix,'L');
           }
           if (number == 2) {
             this.rotateMatrix(this.cube.faceList[1].cellMatrix,'R');
           }
         }
         else{
           if (number == 0) {
             this.rotateMatrix(this.cube.faceList[0].cellMatrix,'R');
           }
           if (number == 2) {
             this.rotateMatrix(this.cube.faceList[1].cellMatrix,'L');
           }
         }
      }
    }
    else if (face == 'cube show-left') {
      if (mode == 'Columns') {
        if (direction == 'Up') {
          if (number == 0) {
            this.rotateMatrix(this.cube.faceList[1].cellMatrix,'L');
          }
          if (number == 2) {
            this.rotateMatrix(this.cube.faceList[0].cellMatrix,'R');
          }
        }
        else{
          if (number == 0) {
            this.rotateMatrix(this.cube.faceList[1].cellMatrix,'R');
          }
          if (number == 2) {
            this.rotateMatrix(this.cube.faceList[0].cellMatrix,'L');
          }
        }
      }
    }
    else if (face == 'cube show-back') {
      if (mode == 'Columns') {
        if (direction == 'Up') {
          if (number == 0) {
            this.rotateMatrix(this.cube.faceList[2].cellMatrix,'L');
          }
          if (number == 2) {
            this.rotateMatrix(this.cube.faceList[3].cellMatrix,'R');
          }
        }
        else{
          if (number == 0) {
            this.rotateMatrix(this.cube.faceList[2].cellMatrix,'R');
          }
          if (number == 2) {
            this.rotateMatrix(this.cube.faceList[3].cellMatrix,'L');
          }
        }
      }
    }
    else if (direction == 'Up') {
       if (number == 0) {
           this.rotateMatrix(this.cube.faceList[3].cellMatrix,'L');
         }
        if (number == 2) {
          this.rotateMatrix(this.cube.faceList[2].cellMatrix,'R');
        }
     }
     else{
        if (number == 0) {
           this.rotateMatrix(this.cube.faceList[3].cellMatrix,'R');
        }
        if (number == 2) {
          this.rotateMatrix(this.cube.faceList[2].cellMatrix,'L');
        }
     }
   }
 }
 rotateMatrix(matrix:Cell[][], direction:string)
 {
  //rotacion derecha
  if (direction == 'R') {
    matrix = matrix.reverse();
    for (var i = 0; i < matrix.length; i++) {
     for (var j = 0; j < i; j++) {
      var temp = matrix[i][j];
      matrix[i][j] = matrix[j][i];
      matrix[j][i] = temp;
      }
    }
  }
  else{
    //rotacion izquierda
    var n = matrix.length;
        for (var i=0; i<n/2; i++) {
            for (var j=i; j<n-i-1; j++) {
                var tmp=matrix[i][j];
                matrix[i][j]=matrix[j][n-i-1];
                matrix[j][n-i-1]=matrix[n-i-1][n-j-1];
                matrix[n-i-1][n-j-1]=matrix[n-j-1][i];
                matrix[n-j-1][i]=tmp;
            }
        }
  } 
  return matrix;  
 }
}

