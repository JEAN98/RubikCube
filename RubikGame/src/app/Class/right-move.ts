import {Faces} from './faces.enum';
import { Instruction } from './instruction';
export class RightMove implements Instruction{	
  star = Faces.Right;
  end = Faces.Back;
  movements = [
    { from: Faces.Front, to: Faces.Right },
    { from: Faces.Left, to: Faces.Front },
    { from: Faces.Back, to: Faces.Left },
  ];
}
