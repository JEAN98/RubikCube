import { Faces } from './faces.enum';
import { Instruction } from './instruction';
export class TopRightMovement implements Instruction{
	star = Faces.Right;
	end = Faces.Bottom;
	movements = [
    	{ from: Faces.Top, to: Faces.Right },
    	{ from: Faces.Left, to: Faces.Top },
    	{ from: Faces.Bottom, to: Faces.Left },
    ];
}
