import {Cell} from './cell';

export class Face {
    public faceID   : number;
    public cellMatrix : Cell[][];
    constructor(faceID:number,cellMatrix:Cell[][]){
        this.faceID   = faceID;
        this.cellMatrix = cellMatrix;
    }
}


// Front  = 0
// right  = 1
// back   = 2
// left   = 3
// top    = 4
// bottom = 5
