import {Faces} from './faces.enum';
export interface Instruction {
  star: Faces;
  end: Faces;
  movements: any[];
}
