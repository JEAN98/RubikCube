import {Faces} from './faces.enum';
import { Instruction } from './instruction';
export class UpMove implements Instruction{
  star = Faces.Top;
  end = Faces.Back;
  movements = [
    { from: Faces.Front, to: Faces.Top },
    { from: Faces.Bottom, to: Faces.Front },
    { from: Faces.Back, to: Faces.Bottom },
  ];
}
