export class Cell {
    public color         : string;
    public idFaceOriginal: number;
    constructor(color:string, idface:number){
        this.color          = color;
        this.idFaceOriginal = idface;
    }
}
