import { Injectable } from '@angular/core';
import {Cube}         from './Class/cube';
import {Face}         from './Class/face';
import {Cell}         from './Class/cell';

@Injectable({
  providedIn: 'root'
})
export class CubeHelperService {
  public cube      : Cube;
  public colorList : string[];
  public tempCube        : Face[];
  public currentCube     : Cube;
  public starX:number;
  public starY:number;
  public mode:string;
  public moveUp:number[] = [];
  public moveRight:number[] = [];
  public stepsList : Cube[];
  constructor() { 
    this.colorList = [ 
          "cell cube__face--front","cell cube__face--back",
          "cell cube__face--right","cell cube__face--left",
          "cell cube__face--top","cell cube__face--bottom"
    ];
    this.buildCube();
    this.moveUp = [0,4,1,5,0];
    this.moveRight = [0,2,1,3,0];
    this.stepsList = [];
    this.readStepsList();
  }

 buildCube(){
    let faceList : Face[];
    faceList = [];
    for (let i = 0; i < 6; i++) {
      let cellMatrix = [];
      for (let k = 0; k < 3; k++) {
          let listCell = []
          for (let j = 0; j < 3; j++) {
            listCell.push(new Cell(this.colorList[i],i))           
          }
          cellMatrix.push(listCell);
      }
      faceList.push(new Face(i,cellMatrix));
    }
    this.cube = new Cube(faceList);
  }

 
  readStepsList(){
    let jsonResult = <Cube[]>JSON.parse(localStorage.getItem("stepsList"));
    this.stepsList = jsonResult != null ? jsonResult : [];
  }

  getLastIndexStep(){
    this.readStepsList();
    return this.stepsList.length - 1;
  }
  
  saveSteps(newStep:Cube){
    this.readStepsList();
    //if(newStep.index == -1){
      //New movement added
      this.stepsList.push(newStep);
      localStorage.setItem("stepsList",JSON.stringify(this.stepsList));
    /*}
    else{
      //Affect old movements
      this.stepsList[newStep.index] = newStep;
      let top = newStep.index;
      let cont = 0;
      let result = [];
      while(cont <= top){
        result.push(this.stepsList[cont]);
        cont++;
      }
      this.stepsList = result;
    }*/
  }

  getMovement(index:number){
    this.readStepsList();
    return index > -1 ? this.stepsList[index] : null;
  }
  
 rotateMatrix(matrix:Cell[][], direction:string)
 {
    //rotacion derecha
    if (direction == 'R') {
      matrix = matrix.reverse();
      for (var i = 0; i < matrix.length; i++) {
        for (var j = 0; j < i; j++) {
            var temp = matrix[i][j];
            matrix[i][j] = matrix[j][i];
            matrix[j][i] = temp;
          }
      }
  }
  else{
    //rotacion izquierda
    var origMatrix = matrix.slice();
    for(var i=0; i < matrix.length; i++) {
      var row = matrix[i].map(function(x, j) {
        var k = (matrix.length - 1) - j;
          return origMatrix[k][i];
      });
    matrix[i] = row;
    }
  } 
  return matrix;  
 }
}


















 /* dragStar(event,cube:Cube){
      debugger;

      this.currentCube = cube;
      this.tempCube = cube;
      this.starX  = event.clientX;
      this.starY = event.clientY;
      if (this.starX > 541 && this.starX < 608) {
        this.row = 0;
      }
      else if (this.starX > 608 && this.starX < 674) {
        this.row = 1;
      }
      else if (this.starX > 674 && this.starX < 739) {
        this.row = 2;
      }
      else if (this.starY > 342 && this.starY < 406) {
        this.column = 0;
      }
      else if (this.starY > 406 && this.starY < 473) {
        this.column = 1;   
      }
      else if (this.starY > 473 && this.starY < 539) {
        this.column = 2;  
      }
  }*/
/*   dragEnd(event) {
     debugger
    console.log(event.pageY + '-' + event.clientX)
    if (this.starX < event.clientX && (event.clientX - this.starX) > 60) {
      console.log('Moving right');
      this.move('R',this.row,this.column);
      return this.currentCube;
    }
    if(this.starX  > event.clientX && (this.starX - event.clientX) > 60){
      console.log('Moving left');
      this.move('L',this.row,this.column);
      return this.currentCube;
    }
    if (this.starY < event.clientY && (event.clientY - this.starY) > 60) {
      console.log('Moving down');
      this.move('D',this.row,this.column);
      return this.currentCube;
    }
    if(this.starY  > event.clientY && (this.starY - event.clientY) > 60){
      console.log('Moving Up'); 
      this.move('U',this.row,this.column);
      return this.currentCube; 
    }
 }*/