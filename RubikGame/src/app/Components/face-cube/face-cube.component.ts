import { Component, OnInit, Input } from '@angular/core';
import {Face} from '../../Class/face';
@Component({
  selector: 'app-face-cube',
  templateUrl: './face-cube.component.html',
  styleUrls: ['./face-cube.component.css']
})
export class FaceCubeComponent implements OnInit {
  @Input() face : string;
  @Input() currentFace : Face;
  constructor() { 
  }

  ngOnInit() {
      console.log( this.currentFace.faceID +"  " + JSON.stringify(this.currentFace.cellMatrix ))
  }

}
