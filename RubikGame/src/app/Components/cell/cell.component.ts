import { Component, OnInit, Input } from '@angular/core';
import {Cell} from '../../Class/cell';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent implements OnInit {
  @Input() currentCell : Cell; 
  constructor() { }

  ngOnInit() {
    console.log("Message from cell compoment" + JSON.stringify(this.currentCell));
  }

}
