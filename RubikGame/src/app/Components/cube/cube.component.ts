import { Component, OnInit, Renderer, ElementRef } from '@angular/core';
import {CubeHelperService}   from '../../cube-helper.service';
import {ServiceCube2Service}   from '../../service-cube2.service';
import {Cube}                from '../../Class/cube';
import { Face } from '../../Class/face';
@Component({
  selector: 'app-cube',
  templateUrl: './cube.component.html',
  styleUrls: ['./cube.component.css']
})
export class CubeComponent implements OnInit {
  public currentMovement : string;
  private styleList      : string[];
  public currentCube     : Cube;
  public face            : Face;
  public num             : number =  0;
  public mode            : string; 
  public direction       : string;
  public newService : ServiceCube2Service;
  public stepIndex : number;
  constructor(private service: CubeHelperService, renderer: Renderer, elementRef: ElementRef){ 
    this.styleList = [
      "cube show-front","cube show-right",
      "cube show-back","cube show-left",
      "cube show-top","cube show-bottom",
    ];
    
    this.face = this.service.cube.faceList[0];
    this.change(0);
    /*renderer.listen(elementRef.nativeElement, 'dragstart', (event) => {
      this.service.dragStar(event,this.currentCube);
    })
    renderer.listen(elementRef.nativeElement, 'dragend', (event) => {
      this.currentCube = this.service.dragEnd(event);
    })
    renderer.listen(elementRef.nativeElement, 'click', (event) => {
      this.service.click(event);
    })*/
  }
  
  ngOnInit() {
    this.startGame();
  }

  startGame(){
    this.currentCube = this.service.cube;
    this.newService = new ServiceCube2Service(this.currentCube);
    this.buildCubeAccordingToLastMovement();
  }

  change(num:number){
    this.currentMovement = this.styleList[num];
  }
  
  move(){
    if(this.num > -1 && this.num < 4){
      this.newService.Core(this.mode,this.direction,this.num,this.currentMovement);
      this.stepIndex = this.newService.getLastIndexStep();
    }
  }

  buildCubeAccordingToLastMovement(){
    this.stepIndex = this.newService.getLastIndexStep();
    if(this.stepIndex == -1){
      this.service.saveSteps(this.currentCube);
    }
    else{
      this.currentCube = this.newService.getMovement(this.stepIndex);
      this.newService.cube = this.currentCube;
    }
  }

  reStart(){
    this.newService.setOriginalState();
    this.service.buildCube();
    this.startGame();
  }

  undo(){
    debugger;
    let step = this.newService.stepsList[this.stepIndex-1];
    if(step!= null){
      this.currentCube = step;
      this.newService.cube = this.currentCube;
      this.stepIndex--;
    }
 }

 abolish(){
  let step = this.newService.stepsList[this.stepIndex+1];
    if(step!= null){
      this.currentCube = step;
      this.newService.cube = this.currentCube;
      this.stepIndex++;
    }
 }
 
}

   

