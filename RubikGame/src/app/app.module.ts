import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CubeComponent } from './Components/cube/cube.component';
import { FaceCubeComponent } from './Components/face-cube/face-cube.component';
import { CellComponent } from './Components/cell/cell.component';
import {CubeHelperService} from './cube-helper.service';

@NgModule({
  declarations: [
    AppComponent,
    CubeComponent,
    FaceCubeComponent,
    CellComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    CubeHelperService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
