import { TestBed, inject } from '@angular/core/testing';

import { ServiceCube2Service } from './service-cube2.service';

describe('ServiceCube2Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServiceCube2Service]
    });
  });

  it('should be created', inject([ServiceCube2Service], (service: ServiceCube2Service) => {
    expect(service).toBeTruthy();
  }));
});
